# setup.py
import pathlib
from setuptools import setup, find_packages

here = pathlib.Path(__file__).parent.resolve()


setup(
    name='Nectar',
    version='0.1',
    package_dir={"nectar" : "src"}
)