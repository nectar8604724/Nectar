#!/usr/bin/env python3
from __future__ import annotations
from enum import Enum, auto
import logging
from pathlib import Path
from typing import List, Optional, Tuple
import msgspec

logger = logging.getLogger(__name__)


class Status(Enum):
    NOT_RUN = auto()
    IN_PROGRESS = auto()
    PASSED = auto()
    FAILED = auto()


class Box:
    name: str
    status: Status

    def __init__(self, name) -> None:
        self.name = name
        self.status = Status.NOT_RUN


class StageBox(Box):
    steps: List[Box] = []

    def __init__(self, stage: NectarPipelineStage) -> None:
        self.name = stage.name
        self.steps = []

        for step in stage:
            self.steps.append(Box(step.name))


class PipelineBoxes(Box):
    stages: List[StageBox] = []

    def __init__(self, pipeline: NectarPipeline) -> None:
        self.name = stage.name
        self.stages = []

        for stage in pipeline:
            self.stages.append(StageBox(stage))


class NectarPipeline(msgspec.Struct):
    name: str
    stages: List[NectarPipelineStage]
    image: Optional[str] = None

    def __post_init__(self):
        for stage in self.stages:

            if not stage.image:
                stage.image = self.image

            for step in stage.steps:
                if not step.image:
                    step.image = stage.image

                if not step.image:
                    raise KeyError(
                        "`image` was not defined for step {}".format(step.name)
                    )

    def __hash__(self):
        return hash((self.name, tuple(self.stages), self.image))

    def __repr__(self):
        return self.name

    @staticmethod
    def _join_wide(items: List[str], concatenator: str = "->", spacing: int = 4) -> str:
        max_height = 0

        # Get max height for all boxes
        for item in items:
            height = item.count("\n")
            max_height = height if height > max_height else max_height

        ret_lines: List[str] = [""] * max_height

        for item in items:
            item = item + "\n" * (max_height - item.count("\n"))
            for i, line in enumerate(item.splitlines()):
                # If it's an empty line, add whitespaces equal to the padded width
                line = " " * item.index("\n") if line == "" else line
                ret_lines[i] = " ".join([ret_lines[i], line])

        return ret_lines

    def dag(self) -> str:
        stages_dag = []
        for stage in self.stages:
            stages_dag.append(stage.dag())

        return NectarPipeline._join_wide(stages_dag)


class NectarPipelineStage(msgspec.Struct):
    name: str
    steps: List[NectarPipelineStep]
    image: Optional[str] = None

    def __hash__(self):
        return hash((self.name, tuple(self.steps), self.image))

    def __repr__(self):
        return self.name

    def __dag_width(self) -> int:
        length = len(self.name)
        for step in self.steps:
            step_len = len(step.name)
            length = step_len if step_len > length else length
        return length

    def dag(self) -> str:
        width = self.__dag_width()
        line_break = "{}".format("━" * (width + 2))
        ret = "┏{}┓\n┃ {}{} ┃\n┣{}┫".format(
            line_break, self.name, " " * (width - len(self.name)), line_break
        )

        for step in self.steps:
            padded_name = step.name + " " * (width - len(step.name))
            ret = "{}\n┃ {} ┃".format(ret, padded_name)
        ret = "{}\n┗{}┛\n".format(ret, line_break)

        return ret


class NectarPipelineStep(msgspec.Struct):
    name: str
    file: str
    image: Optional[str] = None

    def __hash__(self):
        return hash((self.name, self.file, self.image))

    def __repr__(self):
        return self.name


def decode(yaml: str) -> NectarPipeline:
    """Decode a NectarConfig file from TOML"""
    return msgspec.yaml.decode(yaml, type=NectarPipeline)


if __name__ == "__main__":
    text = Path("sample/sample1.yml").read_text()
    pipeline = decode(text)
    # print(pipeline.dag())
    for entry in pipeline.dag():
        print(entry)
