import msgspec


class Base(msgspec.Struct, forbid_unknown_fields=True, rename="lower"):
    pass
