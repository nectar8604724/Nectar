import unittest
from nectar.NectarConfig import decode
import yaml

NO_IMAGES = """
---
name: pipeline-name
stages:
- name: first-stage
  steps:
  - name: first-step
    file: hello_world.sh
"""


TWO_STAGE_SAME_STEP = """
---
name: pipeline-name
image: abc.xyz
stages:
- name: first-stage
  image: abc.xyz
  steps:
  - name: first-step
    file: hello_world.sh
    image: abc.xyz
  - name: second-step
    file: goodbye_world.sh
    image: abc.xyz
- name: second-stage
  image: abc.xyz
  steps:
  - name: first-step
    file: hello_world.sh
    image: abc.xyz
  - name: second-step
    file: goodbye_world.sh
    image: abc.xyz
"""


class TestDefaultImage(unittest.TestCase):
    def test_image_only_at_pipeline(self):
        tmp = yaml.load(NO_IMAGES, Loader=yaml.Loader)
        tmp["image"] = "abc.xyz"
        tmp = yaml.dump(tmp)
        ret = decode(tmp)

        self.assertEqual(ret.stages[0].steps[0].image, "abc.xyz")
        self.assertEqual(ret.stages[0].image, "abc.xyz")

    def test_image_only_at_stage(self):
        tmp = yaml.load(NO_IMAGES, Loader=yaml.Loader)
        tmp["stages"][0]["image"] = "abc.xyz"
        tmp = yaml.dump(tmp)
        ret = decode(tmp)

        self.assertEqual(ret.stages[0].steps[0].image, "abc.xyz")

    def test_no_image_err(self):
        with self.assertRaises(KeyError):
            decode(NO_IMAGES)

    def test_two_steps_in_diff_stages_same_parent(self):
        ret = decode(TWO_STAGE_SAME_STEP)
        print(ret.dag().nodes)
        self.assertFalse()


if __name__ == "__main__":
    unittest.main()
