HERE   := justfile_directory()
PYTHON_DEFAULT := if os() == "windows" { "python" } else { "python3" }
PYTHON := env_var_or_default("PYTHON", PYTHON_DEFAULT)

default:
  @just --list

build:
  pip install . -q

test: build
  {{ PYTHON }} -m unittest discover tests -s src/tests

dev:
  podman build -t nectar .
  podman run -it \
    --mount type=bind,src={{ HERE }}/src,dst=/app/src \
    --mount type=bind,src={{ HERE }}/Justfile,dst=/app/Justfile \
    nectar \
    bash
