FROM python:3.12.8-bullseye
WORKDIR /app
COPY requirements.txt /app
COPY setup.py /app
RUN pip install --no-cache-dir -r requirements.txt
